//
//  ViewController.swift
//  HexaView
//
//  Created by 田高　友和 on 2015/11/22.
//  Copyright © 2015年 HexaForce Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {
    
    let _UIWebView : UIWebView = UIWebView()
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        _UIWebView.delegate = self
        _UIWebView.frame = self.view.bounds
        self.view.addSubview(_UIWebView)
        self.loadRequest("https://www.google.com")
        
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("\(self) " + __FUNCTION__)
        
        // キーボードの開閉を検知したら、下記メソッドを呼ぶようにする
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // Called when the view has been fully transitioned onto the screen. Default does nothing
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("\(self) " + __FUNCTION__)
    }
    
    // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("\(self) " + __FUNCTION__)
    }
    
    // Called after the view was dismissed, covered or otherwise hidden. Default does nothing
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        print("\(self) " + __FUNCTION__)
        
        // 観測を辞める
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    
    // ページが新しいリクエストを発行したときに呼ばれる関数
    func webView(webView: UIWebView,
        shouldStartLoadWithRequest request: NSURLRequest,
        navigationType: UIWebViewNavigationType) -> Bool {
            print("\(self) " + __FUNCTION__)
            
            if let
                url = request.URL,
                components = NSURLComponents(URL: url, resolvingAgainstBaseURL: false),
                scheme = components.scheme,
                host = components.host,
                queryItems = components.queryItems as [NSURLQueryItem]?
            {
                if scheme == "native" {
                    self.execCommand(host, items: queryItems)
                    return false
                }
            }
            
            return true
    }
    
    //
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?){
        print("\(self) " + __FUNCTION__)
    }
    
    //ページが読み終わったときに呼ばれる関数
    func webViewDidFinishLoad(webView: UIWebView) {
        print("\(self) " + __FUNCTION__)
        // JavaScriptの実行結果をStringで受け取ることも可能です
        //        if let title = webView.stringByEvaluatingJavaScriptFromString("document.title;"){
        //            println("page title: \(title)")
        //        }
        //
        //        if let ret = webView.stringByEvaluatingJavaScriptFromString("confirm('知っていました？');"){
        //            println("return value: \(ret)")
        //        }
    }
    
    //ページを読み始めた時に呼ばれる関数
    func webViewDidStartLoad(webView: UIWebView) {
        print("\(self) " + __FUNCTION__)
    }
    
    //
    func loadRequest(url: String) {
        let _URL = NSURL(string: url)
        let _URLRequest = NSURLRequest(URL: _URL!)
        _UIWebView.loadRequest(_URLRequest)
        // ステータスバーのインジケーターを表示
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func execCommand(cmd: String, items:[NSURLQueryItem]) {
        switch cmd {
        case "command":
            if let
                arg = items.filter({ item in item.name == "arg" }).first?.value,
                callback = items.filter({ item in item.name == "callback" }).first?.value
            {
                if let ret = _UIWebView.stringByEvaluatingJavaScriptFromString("\(callback)('「\(arg)」と送りましたね？');"){
                    print("return value: \(ret)")
                }
            }
        default:
            break
        }
    }
    
    // キーボードが開く時に呼ばれます
    func keyboardWillShow(notification: NSNotification?) {
        print("\(self) " + __FUNCTION__)
        // JavaScriptを呼んで、ボタンにdisabled属性をtrueに
        let script = NSMutableString()
        script.appendString("(function(){ var btn = document.querySelector('#trigger');")
        script.appendString("btn.disabled = true; }());")
        _UIWebView.stringByEvaluatingJavaScriptFromString(script as String)
    }
    
    // キーボードが閉じる時に呼ばれます
    func keyboardWillHide(notification: NSNotification?) {
        print("\(self) " + __FUNCTION__)
        // JavaScriptを呼んで、ボタンにdisabled属性をfalseに
        let script = NSMutableString()
        script.appendString("(function(){ var btn = document.querySelector('#trigger');")
        script.appendString("btn.disabled = false; }());")
        _UIWebView.stringByEvaluatingJavaScriptFromString(script as String)
    }
    
    //
    func bundleLocalResource(name: String, type: String){
        // self.bundleLocalResource("index", "html")
        if let localResource = NSBundle.mainBundle().pathForResource(name, ofType: type) {
            self.loadRequest(localResource)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("\(self) " + __FUNCTION__)
        // Dispose of any resources that can be recreated.
        
        
    }
    
}

